<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Service;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\ApartmentDescription;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Service\Authentication\AuthorizationInterface;
use Psr\Http\Message\RequestInterface;

class PriceCalculationClient
{
    private $client;
    private $authorization;
    private $mapper;

    public function __construct(
        ClientInterface $client,
        AuthorizationInterface $authorization,
        Mapper $mapper
    ) {
        $this->client = $client;
        $this->authorization = $authorization;
        $this->mapper = $mapper;
    }

    public function calculateApartmentPrice(ApartmentDescription $description)
    {
        $request = $this->getRequest('price-calculation/rest/v1/calculator/apartment', 'POST');
        $request = $this->addApartmentDescription($description, $request);

        $response = $this->client->send($request);

        return $this->mapper->toPriceCalculationResult(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    private function addApartmentDescription(ApartmentDescription $description, RequestInterface $request)
    {
        $json = \GuzzleHttp\json_encode(
            $this->mapper->fromApartmentDescription($description)
        );

        return $request->withBody(\GuzzleHttp\Psr7\stream_for($json));
    }

    /**
     * @param string $path
     * @param string $method
     * @return Request
     */
    private function getRequest($path, $method)
    {
        $request = new Request($method, $path, ['Content-Type' => 'application/json']);
        return $this->authorization->authorize($request);
    }
}
