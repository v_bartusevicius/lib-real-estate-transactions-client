<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Service;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\ApartmentDescription;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\City;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\PriceCalculationResult;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\Street;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer\ApartmentDescriptionNormalizer;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer\PriceCalculationResultNormalizer;
use Paysera\Component\Serializer\Normalizer\ArrayNormalizer;

class Mapper
{
    private $apartmentDescriptionNormalizer;
    private $priceCalculationResultNormalizer;
    private $citiesNormalizer;
    private $streetsNormalizer;

    public function __construct(
        ApartmentDescriptionNormalizer $apartmentDescriptionNormalizer,
        PriceCalculationResultNormalizer $priceCalculationResultNormalizer,
        ArrayNormalizer $citiesNormalizer,
        ArrayNormalizer $streetsNormalizer
    ) {
        $this->apartmentDescriptionNormalizer = $apartmentDescriptionNormalizer;
        $this->priceCalculationResultNormalizer = $priceCalculationResultNormalizer;
        $this->citiesNormalizer = $citiesNormalizer;
        $this->streetsNormalizer = $streetsNormalizer;
    }

    public function toApartmentDescription(array $data)
    {
        return $this->apartmentDescriptionNormalizer->mapToEntity($data);
    }

    public function fromApartmentDescription(ApartmentDescription $apartmentDescription)
    {
        return $this->apartmentDescriptionNormalizer->mapFromEntity($apartmentDescription);
    }

    public function toPriceCalculationResult(array $data)
    {
        return $this->priceCalculationResultNormalizer->mapToEntity($data);
    }

    public function fromPriceCalculationResult(PriceCalculationResult $priceCalculationResult)
    {
        return $this->priceCalculationResultNormalizer->mapFromEntity($priceCalculationResult);
    }

    /**
     * @param array $data
     * @return City[]
     */
    public function toCitiesList(array $data)
    {
        return $this->citiesNormalizer->mapToEntity($data);
    }

    /**
     * @param array $data
     * @return City
     */
    public function toCity(array $data)
    {
        return $this->citiesNormalizer->mapToEntity([$data])[0];
    }

    /**
     * @param array $data
     * @return Street[]
     */
    public function toStreetList(array $data)
    {
        return $this->streetsNormalizer->mapToEntity($data);
    }

    /**
     * @param array $data
     * @return Street
     */
    public function toStreet(array $data)
    {
        return $this->streetsNormalizer->mapToEntity([$data])[0];
    }
}
