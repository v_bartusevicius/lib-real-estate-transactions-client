<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Service\Authentication;

use GuzzleHttp\Psr7\Request;

class Bearer implements AuthorizationInterface
{
    private $authToken;

    /**
     * @param string $authToken
     */
    public function __construct($authToken)
    {
        $this->authToken = $authToken;
    }

    public function authorize(Request $request)
    {
        return $request->withAddedHeader(
            'Authorization',
            sprintf('Bearer %s', $this->authToken)
        );
    }
}
