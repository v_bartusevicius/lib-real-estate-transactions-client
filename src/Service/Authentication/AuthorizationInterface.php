<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Service\Authentication;

use GuzzleHttp\Psr7\Request;

interface AuthorizationInterface
{
    /**
     * @param Request $request
     * @return Request
     */
    public function authorize(Request $request);
}
