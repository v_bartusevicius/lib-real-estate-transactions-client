<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Service;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\City;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\Street;
use OberHaus\Bundle\RealEstateTransactionsClientBundle\Service\Authentication\AuthorizationInterface;

class AddressClient
{
    private $client;
    private $authorization;
    private $mapper;

    public function __construct(
        ClientInterface $client,
        AuthorizationInterface $authorization,
        Mapper $mapper
    ) {
        $this->client = $client;
        $this->authorization = $authorization;
        $this->mapper = $mapper;
    }

    public function getCities()
    {
        $request = $this->getRequest('address/rest/v1/cities', 'GET');
        $response = $this->client->send($request);

        return $this->mapper->toCitiesList(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    /**
     * @param string $cityId
     * @return City
     */
    public function getCity($cityId)
    {
        $request = $this->getRequest(
            sprintf('address/rest/v1/cities/%d', urlencode($cityId)),
            'GET'
        );
        $response = $this->client->send($request);

        return $this->mapper->toCity(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    /**
     * @param string $cityId
     * @return Street[]
     */
    public function getStreets($cityId)
    {
        $request = $this->getRequest(
            sprintf('address/rest/v1/cities/%d/streets', urlencode($cityId)),
            'GET'
        );
        $response = $this->client->send($request);

        return $this->mapper->toStreetList(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    /**
     * @param string $cityId
     * @param string $slug
     * @return Street[]
     */
    public function autocompleteStreets($cityId, $slug)
    {
        $request = $this->getRequest(
            sprintf(
                'address/rest/v1/cities/%d/streets/autocomplete/%s',
                urlencode($cityId),
                urlencode($slug)
            ),
            'GET'
        );
        $response = $this->client->send($request);

        return $this->mapper->toStreetList(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    /**
     * @param string $cityId
     * @param string $streetId
     * @return Street
     */
    public function getStreet($cityId, $streetId)
    {
        $request = $this->getRequest(
            sprintf(
                'address/rest/v1/cities/%d/streets/%d',
                urlencode($cityId),
                urlencode($streetId)
            ),
            'GET'
        );
        $response = $this->client->send($request);

        return $this->mapper->toStreet(
            \GuzzleHttp\json_decode($response->getBody()->getContents(), true)
        );
    }

    /**
     * @param string $path
     * @param string $method
     * @return Request
     */
    private function getRequest($path, $method)
    {
        $request = new Request($method, $path, ['Content-Type' => 'application/json']);
        return $this->authorization->authorize($request);
    }
}
