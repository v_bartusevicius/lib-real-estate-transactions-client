<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $root = $treeBuilder->root('ober_haus_real_estate_transactions_client');

        $root
            ->children()
                ->scalarNode('base_uri')->defaultNull()->end()
                ->scalarNode('token')->defaultNull()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
