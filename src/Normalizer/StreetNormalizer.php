<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\Street;
use Paysera\Component\Serializer\Normalizer\DenormalizerInterface;
use Paysera\Component\Serializer\Normalizer\NormalizerInterface;

class StreetNormalizer implements NormalizerInterface, DenormalizerInterface
{
    private $cityNormalizer;

    public function __construct(CityNormalizer $cityNormalizer)
    {
        $this->cityNormalizer = $cityNormalizer;
    }

    /**
     * @param array $data
     *
     * @return Street
     */
    public function mapToEntity($data)
    {
        $street = new Street();

        if (isset($data['name'])) {
            $street->setName($data['name']);
        }
        if (isset($data['id'])) {
            $street->setId($data['id']);
        }
        if (isset($data['city'])) {
            $street->setCity($this->cityNormalizer->mapToEntity($data['city']));
        }

        return $street;
    }

    /**
     * @param Street $entity
     *
     * @return array
     */
    public function mapFromEntity($entity)
    {
        $data = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'city' => $this->cityNormalizer->mapFromEntity($entity->getCity()),
        ];

        return array_filter($data);
    }
}
