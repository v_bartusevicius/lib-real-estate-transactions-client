<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\Location;
use Paysera\Component\Serializer\Normalizer\DenormalizerInterface;
use Paysera\Component\Serializer\Normalizer\NormalizerInterface;

class LocationNormalizer implements NormalizerInterface, DenormalizerInterface
{
    private $streetNormalizer;

    public function __construct(StreetNormalizer $streetNormalizer)
    {
        $this->streetNormalizer = $streetNormalizer;
    }

    /**
     * @param array $data
     *
     * @return Location
     */
    public function mapToEntity($data)
    {
        $location = new Location();

        if (isset($data['street'])) {
            $location->setStreet($this->streetNormalizer->mapToEntity($data['street']));
        }
        if (isset($data['building_number'])) {
            $location->setBuildingNumber($data['building_number']);
        }

        return $location;
    }

    /**
     * @param Location $entity
     *
     * @return array
     */
    public function mapFromEntity($entity)
    {
        $data = [
            'street' => $this->streetNormalizer->mapFromEntity($entity->getStreet()),
            'building_number' => $entity->getBuildingNumber(),
        ];

        return array_filter($data);
    }
}
