<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\City;
use Paysera\Component\Serializer\Normalizer\DenormalizerInterface;
use Paysera\Component\Serializer\Normalizer\NormalizerInterface;

class CityNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param array $data
     *
     * @return City
     */
    public function mapToEntity($data)
    {
        $city = new City();

        if (isset($data['name'])) {
            $city->setName($data['name']);
        }
        if (isset($data['id'])) {
            $city->setId($data['id']);
        }

        return $city;
    }

    /**
     * @param City $entity
     *
     * @return array
     */
    public function mapFromEntity($entity)
    {
        $data = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
        ];

        return array_filter($data);
    }
}
