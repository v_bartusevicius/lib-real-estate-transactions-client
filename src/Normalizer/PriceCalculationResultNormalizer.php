<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\PriceCalculationResult;
use Paysera\Component\Serializer\Normalizer\DenormalizerInterface;
use Paysera\Component\Serializer\Normalizer\NormalizerInterface;

class PriceCalculationResultNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param array $data
     *
     * @return PriceCalculationResult
     */
    public function mapToEntity($data)
    {
        $calcResult = new PriceCalculationResult();

        if (isset($data['calculated_price'])) {
            $calcResult->setCalculatedPrice($data['calculated_price']);
        }
        if (isset($data['precision'])) {
            $calcResult->setPrecision($data['precision']);
        }
        if (isset($data['conditions'])) {
            $calcResult->setConditions($data['conditions']);
        }

        return $calcResult;
    }

    /**
     * @param PriceCalculationResult $entity
     *
     * @return array
     */
    public function mapFromEntity($entity)
    {
        $data = [
            'calculated_price' => $entity->getCalculatedPrice(),
            'precision' => $entity->getPrecision(),
            'conditions' => $entity->getConditions(),
        ];

        return $data;
    }
}
