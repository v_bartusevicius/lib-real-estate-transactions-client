<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Normalizer;

use OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity\ApartmentDescription;
use Paysera\Component\Serializer\Normalizer\DenormalizerInterface;
use Paysera\Component\Serializer\Normalizer\NormalizerInterface;

class ApartmentDescriptionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    private $locationNormalizer;

    public function __construct(
        LocationNormalizer $locationNormalizer
    ) {
        $this->locationNormalizer = $locationNormalizer;
    }

    /**
     * @param array $data
     *
     * @return ApartmentDescription
     */
    public function mapToEntity($data)
    {
        $calcRequest = new ApartmentDescription();

        if (isset($data['rooms_count'])) {
            $calcRequest->setRoomsCount($data['rooms_count']);
        }
        if (isset($data['area'])) {
            $calcRequest->setArea($data['area']);
        }
        if (isset($data['built_epoch'])) {
            $calcRequest->setBuiltEpoch($data['built_epoch']);
        }
        if (isset($data['location'])) {
            $calcRequest->setLocation($this->locationNormalizer->mapToEntity($data['location']));
        }
        if (isset($data['walls'])) {
            $calcRequest->setWalls($data['walls']);
        }

        return $calcRequest;
    }

    /**
     * @param ApartmentDescription $entity
     *
     * @return array
     */
    public function mapFromEntity($entity)
    {
        $data = [
            'rooms_count' => $entity->getRoomsCount(),
            'area' => $entity->getArea(),
            'built_epoch' => $entity->getBuiltEpoch(),
            'location' => $this->locationNormalizer->mapFromEntity($entity->getLocation()),
            'walls' => $entity->getWalls(),
        ];

        return array_filter($data);
    }
}
