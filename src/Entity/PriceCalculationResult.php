<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity;

class PriceCalculationResult
{
    /**
     * @var string
     */
    private $calculatedPrice;

    /**
     * @var int
     */
    private $precision;

    /**
     * @var array
     */
    private $conditions;

    public function __construct()
    {
        $this->conditions = [];
    }

    /**
     * @return string
     */
    public function getCalculatedPrice()
    {
        return $this->calculatedPrice;
    }

    /**
     * @param string $calculatedPrice
     * @return $this
     */
    public function setCalculatedPrice($calculatedPrice)
    {
        $this->calculatedPrice = $calculatedPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * @param int $precision
     * @return $this
     */
    public function setPrecision($precision)
    {
        $this->precision = $precision;
        return $this;
    }

    /**
     * @return array
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param array $conditions
     * @return $this
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
        return $this;
    }
}
