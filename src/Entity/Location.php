<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity;

class Location
{
    /**
     * @var Street
     */
    private $street;

    /**
     * @var string
     */
    private $buildingNumber;

    /**
     * @return Street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param Street $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuildingNumber()
    {
        return $this->buildingNumber;
    }

    /**
     * @param string $buildingNumber
     * @return $this
     */
    public function setBuildingNumber($buildingNumber)
    {
        $this->buildingNumber = $buildingNumber;
        return $this;
    }
}
