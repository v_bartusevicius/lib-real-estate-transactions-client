<?php

namespace OberHaus\Bundle\RealEstateTransactionsClientBundle\Entity;

class ApartmentDescription
{
    const BUILT_SINCE_2000 = 'since_2000';
    const BUILT_TILL_2000 = 'till_2000';
    const BUILT_TILL_1940 = 'till_1940';

    const WALLS_BRICKS = 'bricks';
    const WALLS_CONCRETE = 'concrete';
    const WALLS_WOOD = 'wood';

    /**
     * @var int
     */
    private $roomsCount;

    /**
     * @var float
     */
    private $area;

    /**
     * @var string
     */
    private $builtEpoch;

    /**
     * @var Location
     */
    private $location;

    /**
     * @var string
     */
    private $walls;

    /**
     * @return int
     */
    public function getRoomsCount()
    {
        return $this->roomsCount;
    }

    /**
     * @param int $roomsCount
     * @return $this
     */
    public function setRoomsCount($roomsCount)
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param float $area
     * @return $this
     */
    public function setArea($area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuiltEpoch()
    {
        return $this->builtEpoch;
    }

    /**
     * @param string $builtEpoch
     *
     * @return ApartmentDescription
     */
    public function setBuiltEpoch($builtEpoch)
    {
        $this->builtEpoch = $builtEpoch;
        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getWalls()
    {
        return $this->walls;
    }

    /**
     * @param string $walls
     *
     * @return $this
     */
    public function setWalls($walls)
    {
        $this->walls = $walls;
        return $this;
    }
}
